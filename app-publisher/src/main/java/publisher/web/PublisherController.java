package publisher.web;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;
import publisher.model.Publisher;
import publisher.service.PublisherService;



import java.util.List;
import java.util.Optional;

@RestController
@RequiredArgsConstructor
@Slf4j
public class PublisherController {

    private final PublisherService publisherService;

    @GetMapping("/publishers")
    List<PublisherDTO> getPublishers(@RequestHeader(value = "custom-header", required = false) String customHeader){
        log.info("custom header: {}", customHeader);
        return publisherService.getPublishers().stream()
                .map(PublisherDTO::fromData)
                .toList();
    }

    @GetMapping("/publishers/{publisherId}")
    ResponseEntity<PublisherDTO> getPublisher(@PathVariable int publisherId){
        Publisher publisher = publisherService.getPublisher(publisherId);

        return publisher!=null
                ? ResponseEntity.ok(PublisherDTO.fromData(publisher))
                : ResponseEntity.notFound().build();
    }

}
