package publisher;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;
import publisher.service.PublisherService;

//@Component
@Slf4j
@RequiredArgsConstructor
public class PublisherRunner implements CommandLineRunner {

    private final PublisherService publisherService;
    @Override
    public void run(String... args) throws Exception {
        publisherService.getPublishers().forEach(publisher -> log.info("{}", publisher));
    }
}
