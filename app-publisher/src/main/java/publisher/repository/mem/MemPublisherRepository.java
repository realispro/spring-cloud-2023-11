/*
package publisher.repository.mem;

import org.springframework.stereotype.Repository;
import publisher.model.Publisher;
import publisher.repository.PublisherRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class MemPublisherRepository implements PublisherRepository {

    private static final List<Publisher> PUBLISHERS = new ArrayList<>();

    static {
        PUBLISHERS.add(create("Helion", "http://www.komputerswiat.pl/media/902570/helion-logo-panorama.jpg"));
        PUBLISHERS.add(create("PWN", "http://it.pwn.pl/design/plain_site/images/logo-it-pwn.png"));
    }

    private static int getNextId(){
        return PUBLISHERS.stream().mapToInt(Publisher::getId).max().orElse(0) + 1;
    }

    private static Publisher create(String name, String logo){
        Publisher publisher = new Publisher();
        publisher.setId(getNextId());
        publisher.setName(name);
        publisher.setLogo(logo);
        return publisher;
    }

    @Override
    public List<Publisher> findAll() {
        return new ArrayList<>(PUBLISHERS);
    }

    @Override
    public Optional<Publisher> findById(Integer id) {
        return PUBLISHERS.stream().filter(publisher -> publisher.getId()==id).findFirst();
    }

    @Override
    public Publisher save(Publisher p) {
        p.setId(getNextId());
        PUBLISHERS.add(p);
        return p;
    }
}
*/
