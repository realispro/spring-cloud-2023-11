/*
package publisher.repository.jpa;

import jakarta.persistence.EntityManager;
import jakarta.persistence.PersistenceContext;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Repository;
import publisher.model.Publisher;
import publisher.repository.PublisherRepository;

import java.util.List;
import java.util.Optional;

@Repository
@Primary
public class JpaPublisherRepository implements PublisherRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Publisher> findAll() {
        return entityManager.createQuery("select p from Publisher p", Publisher.class)
                .getResultList();
    }

    @Override
    public Optional<Publisher> findById(int id) {
        return Optional.ofNullable(entityManager.find(Publisher.class, id));
    }

    @Override
    public Publisher save(Publisher p) {
        entityManager.persist(p);
        return p;
    }
}
*/
