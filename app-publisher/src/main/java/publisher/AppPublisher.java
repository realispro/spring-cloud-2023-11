package publisher;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@Slf4j
@SpringBootApplication
@EnableJpaRepositories(basePackages = "publisher.repository")
public class AppPublisher {

    public static void main(String[] args) {
        SpringApplication.run(AppPublisher.class, args);
    }

    /*public static void main(String[] args) {

        ApplicationContext context = new AnnotationConfigApplicationContext("publisher");

        PublisherService publisherService = context.getBean(PublisherService.class);
        publisherService.getPublishers().forEach(publisher->log.info("{}", publisher));
    }*/
}
