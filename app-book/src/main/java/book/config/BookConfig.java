package book.config;

import io.github.resilience4j.circuitbreaker.CircuitBreakerConfig;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JCircuitBreakerFactory;
import org.springframework.cloud.circuitbreaker.resilience4j.Resilience4JConfigBuilder;
import org.springframework.cloud.client.circuitbreaker.CircuitBreaker;
import org.springframework.cloud.client.circuitbreaker.CircuitBreakerFactory;
import org.springframework.cloud.client.circuitbreaker.Customizer;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.security.config.annotation.method.configuration.EnableMethodSecurity;
import org.springframework.security.oauth2.server.resource.authentication.JwtAuthenticationConverter;
import org.springframework.security.oauth2.server.resource.authentication.JwtGrantedAuthoritiesConverter;
import org.springframework.web.client.RestTemplate;

@Configuration
@EnableMethodSecurity
public class BookConfig {

    @LoadBalanced
    @Bean
    RestTemplate restTemplate(){
        return new RestTemplate();
    }

    @Bean
    Customizer<Resilience4JCircuitBreakerFactory> globalCustomConfiguration(){

        CircuitBreakerConfig config = CircuitBreakerConfig.custom()
                .failureRateThreshold(80)
                .slidingWindow(3, 5, CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                //.slowCallDurationThreshold(Duration.ofMillis(10_000))
                //.slidingWindowType(CircuitBreakerConfig.SlidingWindowType.COUNT_BASED)
                //.slidingWindowSize(5)
                .build();

        return factory -> factory.configureDefault(name -> new Resilience4JConfigBuilder(name)
                .circuitBreakerConfig(config)
                .build());

    }

    @Bean
    @Scope("singleton")
    CircuitBreaker circuitBreakerPublisher(CircuitBreakerFactory cbf){
        return cbf.create("app-publisher");
    }

    /*@Bean
    public Client feignClient() throws Exception {
        log.info("Configuring SSL Context for Feign Client");
        return new Client.Default(createSSLContext(), SSLConnectionSocketFactory.getDefaultHostnameVerifier());
    }*/

    @Bean
    JwtAuthenticationConverter jwtAuthenticationConverter(){
        JwtGrantedAuthoritiesConverter grantedAuthoritiesConverter = new JwtGrantedAuthoritiesConverter();
        grantedAuthoritiesConverter.setAuthoritiesClaimName("authorities");
        grantedAuthoritiesConverter.setAuthorityPrefix("ROLE_");

        JwtAuthenticationConverter converter = new JwtAuthenticationConverter();
        converter.setJwtGrantedAuthoritiesConverter(grantedAuthoritiesConverter);
        return converter;
    }


}
