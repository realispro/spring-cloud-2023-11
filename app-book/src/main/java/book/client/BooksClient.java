package book.client;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.hateoas.client.Hop;
import org.springframework.hateoas.client.Traverson;
import org.springframework.hateoas.server.core.TypeReferences;

import java.net.URI;
import java.util.LinkedHashMap;
import java.util.Map;

@Slf4j
public class BooksClient {

    public static void main(String[] args) {

        Traverson traverson = new Traverson(URI.create("http://localhost:8002/hateoas"), MediaTypes.HAL_JSON);
        ParameterizedTypeReference<EntityModel<BookDTO>> bookType = new ParameterizedTypeReference<>() {};
        TypeReferences.CollectionModelType<BookDTO> bookCollectionType = new TypeReferences.CollectionModelType<>() {};
        TypeReferences.CollectionModelType<AuthorDTO> authorCollectionType = new TypeReferences.CollectionModelType<>() {};

        CollectionModel<BookDTO> response
        //CollectionModel<AuthorDTO> response
        //EntityModel<BookDTO> response
                = traverson
                .follow("books")
                //.follow("$._embedded.*._links.author")
                .toObject(bookCollectionType);
        log.info("collection: {}", response.getContent());
    }

    @Data
    private static class BookDTO{
        private String title;
        private String cover;
        private float price;
        private int publisherId;
    }

    @Data
    private static class AuthorDTO{
        private String firstName;
        private String lastName;
    }
}
