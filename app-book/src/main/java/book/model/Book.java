package book.model;

import jakarta.persistence.*;
import lombok.Data;


@Data
@Entity
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String title;
    @ManyToOne
    private Author author; // _id
    private String cover;
    private float price;
    private int publisherId;
}
