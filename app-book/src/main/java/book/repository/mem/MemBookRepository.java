/*
package book.repository.mem;

import book.model.Author;
import book.model.Book;
import book.repository.BookRepository;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Repository
public class MemBookRepository implements BookRepository {

    private static final List<Book> BOOKS = new ArrayList<>();

    static {
        Author author1 = create(1, "Alberto", "Boschetti");
        Author author2 = create(2, "Adam", "Roman");
        Author author3 = create(3, "Sanjoy", "Dasgupta");
        Author author4 = create(4, "Krzysztof", "Liderman");

        BOOKS.add(create(
                "Python. Podstawy nauki o danych.",
                "https://static01.helion.com.pl/global/okladki/326x466/cce76081c5c79abe2b80a93b1d32a5e5,pypod2.jpg",
                59,
                author1,
                1));
        BOOKS.add(create(
                "Testowanie i jakosc oprogramowania",
                "https://static01.helion.com.pl/global/okladki/326x466/a89754a151df50f28adc111b40af7d4e,e_1oe0.jpg",
                39,
                author2,
                2));
        BOOKS.add(create(
                "Algorytmy",
                "https://emp-scs-uat.img-osdw.pl/img-p/1/kipwn/c0aac775/std/e6-172/833542239o.jpg",
                19,
                author3,
                2));
        BOOKS.add(create(
                "Bezpieczenstwo informacyjne",
                "http://it.pwn.pl/var/plain_site/storage/images/media/images/bezpieczestwo-informacyjne_167865/1563-1-pol-PL/bezpieczestwo-informacyjne_167865_article_list_article_thumb.jpg",
                19,
                author4,
                2));
    }


    private static int getNextId(){
        return BOOKS.stream().mapToInt(Book::getId).max().orElse(0) + 1;
    }

    private static Book create(String title, String cover, float price, Author author, int publisherId){
        Book book = new Book();
        book.setId(getNextId());
        book.setTitle(title);
        book.setCover(cover);
        book.setPrice(price);
        book.setAuthor(author);
        book.setPublisherId(publisherId);
        return book;
    }

    private static Author create(int id, String firstName, String lastName){
        Author author = new Author();
        author.setId(id);
        author.setFirstName(firstName);
        author.setLastName(lastName);
        return author;
    }

    @Override
    public List<Book> findAll() {
        return new ArrayList<>(BOOKS);
    }

    @Override
    public Optional<Book> findById(int id) {
        return BOOKS.stream().filter(book->book.getId()==id).findFirst();
    }

    @Override
    public List<Book> findByAuthorId(int authorId) {
        return BOOKS.stream().filter(book -> book.getAuthor().getId()==authorId).toList();
    }

    @Override
    public Book save(Book b) {
        b.setId(getNextId());
        BOOKS.add(b);
        return b;
    }
}
*/
