package book;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@Slf4j
@SpringBootApplication
@EnableFeignClients
public class AppBook {

    public static void main(String[] args) {
        SpringApplication.run(AppBook.class, args);
    }

    /*public static void main(String[] args) {
        BookRepository bookRepository = new MemBookRepository();
        BookService bookService = new BookService(bookRepository);
        bookService.getBooksByAuthor(1).forEach(book->log.info("{}", book));
    }*/
}
