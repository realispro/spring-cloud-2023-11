package book.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@Slf4j
public class ParanoidPublisherServiceFallback implements PublisherService{
    @Override
    public PublisherDTO getPublisher(int publisherId) {
        throw new IllegalArgumentException("problem handled from fallback class");
    }

    @Override
    public List<PublisherDTO> getPublishers(String headerValue) {
        log.error("returning empty publisher list");
        return List.of();
    }
}
