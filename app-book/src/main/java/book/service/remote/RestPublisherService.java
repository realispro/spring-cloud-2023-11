package book.service.remote;

import book.service.PublisherDTO;
import book.service.PublisherService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

//@Component
@RequiredArgsConstructor
@Slf4j
public abstract class RestPublisherService implements PublisherService {

    private final RestTemplate restTemplate;
    //private final CircuitBreaker circuitBreakerPublisher;

    @Override
    @io.github.resilience4j.circuitbreaker.annotation.CircuitBreaker(name="publisher", fallbackMethod = "getPublisherFallback")
    public PublisherDTO getPublisher(int publisherId) {

        String uri = "http://app-publisher" + "/publishers/" + publisherId;
        log.info("about to exchange info using url {}", uri);

        ResponseEntity<PublisherDTO> responseEntity = restTemplate.exchange(
                                uri,
                                HttpMethod.GET,
                                HttpEntity.EMPTY,
                                PublisherDTO.class
                        );

        return responseEntity.getBody();
    }

    public PublisherDTO getPublisherFallback(int publisherId, Throwable t) {
        throw new IllegalArgumentException("problem while communicating with app-publisher", t);
    }
}
