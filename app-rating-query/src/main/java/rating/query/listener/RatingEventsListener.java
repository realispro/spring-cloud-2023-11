package rating.query.listener;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;
import rating.command.event.RatingCommandEvent;
import rating.query.model.Rating;
import rating.query.repository.RatingRepository;

import java.util.function.Consumer;

@Component
@Slf4j
@RequiredArgsConstructor
public class RatingEventsListener {

    private final RatingRepository repository;

    //@KafkaListener(topics="rating-query-topic", groupId = "product-event-group")
    void handleRatingCommand(RatingCommandEvent event){
        log.info("rating query event received: {}", event);
        switch (event.getCommandType()){
            case RATING_CREATE -> {
                Rating rating = new Rating();
                rating.setId(event.getRatingId());
                rating.setBookId(event.getBookId());
                rating.setRate(event.getRate());
                rating.setReview(event.getReview());
                repository.save(rating);
                log.info("rating created: {}", rating);
            }
            case RATING_UPDATE -> {
                Rating rating = repository.findById(event.getRatingId()).orElseThrow();
                rating.setRate(event.getRate());
                rating.setReview(event.getReview());
                repository.save(rating);
                log.info("rating updated: {}", rating);
            }
            default -> log.error("unknown event type");
        }

    }

    @Bean
    Consumer<RatingCommandEvent> consumeRating(){
        return this::handleRatingCommand;
    }
}
