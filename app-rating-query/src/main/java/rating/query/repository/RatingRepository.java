package rating.query.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import rating.query.model.Rating;

import java.util.List;

public interface RatingRepository extends JpaRepository<Rating, Long> {

    List<Rating> findAllByBookId(int movieId);
}
