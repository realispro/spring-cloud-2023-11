package rating.query.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Rating {

    @Id
    private long id;
    private float rate;
    private String review;
    private int bookId;

}
