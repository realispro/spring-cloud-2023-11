package rating.query.web;

import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import rating.query.model.Rating;
import rating.query.repository.RatingRepository;

import java.util.List;

@RestController
@RequiredArgsConstructor
public class RatingController {

    private final RatingRepository repository;

    @GetMapping("/ratings")
    List<Rating> getRatings(@RequestParam(value = "bookId", required = false) Integer bookId){

        return bookId!=null
                ? repository.findAllByBookId(bookId)
                : repository.findAll();
    }

    @GetMapping("/ratings/{ratingId}")
    ResponseEntity<Rating> getRating(@PathVariable long ratingId){
        return ResponseEntity.of(repository.findById(ratingId));
    }

}
