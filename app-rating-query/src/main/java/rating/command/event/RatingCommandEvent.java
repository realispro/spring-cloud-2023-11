package rating.command.event;

import lombok.Data;

@Data
public class RatingCommandEvent {

    private RatingCommandType commandType;
    private long ratingId;
    private int bookId;
    private float rate;
    private String review;

    public enum RatingCommandType {
        RATING_CREATE,
        RATING_UPDATE
    }
}
