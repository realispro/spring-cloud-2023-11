db = db.getSiblingDB('librarian');

db.createUser({
    user: 'admin',
    pwd: 'admin',
    roles: [
        {
            role: 'readWrite',
            db: 'admin',
        },
    ],
});

db.createCollection('ratings', {capped: true, size: 100000});
