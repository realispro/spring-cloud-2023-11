package rating.command.model;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class Rating {

    private Long id;
    private int bookId;
    private float rate;
    private String review;
    private String commenter;
}
