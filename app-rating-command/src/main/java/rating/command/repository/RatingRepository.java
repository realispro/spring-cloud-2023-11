package rating.command.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.CrudRepository;
import rating.command.model.Rating;

public interface RatingRepository extends MongoRepository<Rating, Integer> {
}
