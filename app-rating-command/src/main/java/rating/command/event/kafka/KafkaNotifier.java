package rating.command.event.kafka;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.function.StreamBridge;
import org.springframework.stereotype.Service;
import rating.command.event.RatingCommandEvent;
import rating.command.event.RatingCommandNotifier;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaNotifier implements RatingCommandNotifier {

    //
    private final StreamBridge streamBridge;

    @Override
    public void emit(RatingCommandEvent event) {
        log.info("about to emit event {}", event);
        //kafkaTemplate.send("rating-query-topic", event);
        streamBridge.send("output", event);
    }
}
