package rating.command.event;

public enum RatingCommandType {

    RATING_CREATE,
    RATING_UPDATE
}
