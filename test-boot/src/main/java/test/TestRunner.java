package test;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Slf4j
@RequiredArgsConstructor
@Component
public class TestRunner implements CommandLineRunner {

    @Value("${hello.name}")
    private String name;

    @Override
    public void run(String... args) throws Exception {
        log.info("Hey " + name + "!");
    }
}
